//
//  PointsTablePresenter.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 25.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import Foundation

protocol PointsTablePresenterProtocol {
    init(points: [Point])
    func numberOfElements() -> Int
    func textForElementAtIndexPath(index: Int) -> String
}

struct PointsTablePresenter: PointsTablePresenterProtocol {
    let points: [Point]
    
    init(points: [Point]) {
        self.points = points
    }
    
    func numberOfElements() -> Int {
        return self.points.count
    }
    
    func textForElementAtIndexPath(index: Int) -> String {
        let point = points[index]
        return "[" + "\(point.x)" + "; " + "\(point.y)" + "]"
    }
}
