//
//  Interpolator.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 10.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import Foundation

private let EPSILON: Double = Double.ulpOfOne

struct Spline {
    private typealias Segment = (Point, Point, Point, Point)
    
    private static let Precision: Int = 32
    
    private let firstControlPoint: Point!
    private let lastControlPoint: Point!
    private let count: Int
    private var segments: [Segment]? = nil
    
    var numberOfPoints: Int {
        get {
            if(count > 2) {
                return (count - 1) * Self.Precision + 1
            }
            else {
                return count
            }
        }
    }
    
    init(withControlPoints points: [Point]) {
        self.count = points.count
        self.firstControlPoint = points.first
        self.lastControlPoint = points.last
        self.segments = calculateSpline(points)
    }
    
    func getPoint(atIndex index: Int) -> Point {
        if let locSegments = segments {
            if(index == numberOfPoints - 1) {
                return lastControlPoint
            }
            else {
                let i = index / Self.Precision
                let j = index % Self.Precision
            
                return segmentCalc(locSegments[i], Double(j) / Double(Self.Precision));
            }
        }
        else {
            switch count {
                case 1:
                    return firstControlPoint
                case 2:
                    if(index == 0) {
                        return firstControlPoint
                    }
                    else {
                        return lastControlPoint
                    }
                default:
                    return Point.zero
            }
        }
    }
    
    private func calculateSpline(_ values: [Point]) -> [Segment]? {
        let valuesSize = values.count
        guard valuesSize > 2 else {
            return nil
        }
        
        let n = valuesSize - 1
        var result: [Segment] = []
        
        var tgL = Point.zero
        var tgR = Point.zero
        var cur = Point.zero
        var next = values[1] - values[0]
        var tmpPoint = next.normalizedPoint()
        
        var l1: Double = 0.0
        var l2: Double = 0.0
        var tmp: Double = 0.0
        var x: Double = 0.0
        
        for i in 0..<n {
            let pointi = values[i]
            let pointi1 = values[i + 1]
            var segment: Segment = (pointi, pointi, pointi1, pointi1)
            
            cur = next
            tgL = tgR
            
            if(i+1 < n) {
                next = values[i + 2] - pointi1
                next.normalize()
                
                tgR = cur + next
                tgR.normalize()
            }
            else {
                tgR = Point.zero
            }
            
            if(fabs(pointi1.y - pointi.y) < EPSILON) {
                l1 = 0.0
                l2 = 0.0
            }
            else {
                tmp = pointi1.x - pointi.x
                l1 = fabs(tgL.x) > EPSILON ? tmp / (2.0 * tgL.x) : 1.0
                l2 = fabs(tgR.x) > EPSILON ? tmp / (2.0 * tgR.x) : 1.0
            }
            
            if (fabs(tgL.x) > EPSILON && fabs(tgR.x) > EPSILON) {
                tmp = tgL.y / tgL.x - tgR.y / tgR.x
                if (fabs(tmp) > EPSILON) {
                    x = (pointi1.y - tgR.y / tgR.x * pointi1.x - pointi.y + tgL.y / tgL.x * pointi.x) / tmp
                    if (x > pointi.x && x < pointi1.x) {
                        if (tgL.y > 0.0) {
                            if (l1 > l2) {
                                l1 = 0.0
                            }
                            else {
                                l2 = 0.0
                            }
                        }
                        else {
                            if (l1 < l2) {
                                l1 = 0.0
                            }
                            else {
                                l2 = 0.0
                            }
                        }
                    }
                }
            }
            tmpPoint = tgL * l1
            segment.1 = segment.1 + tmpPoint
            tmpPoint = tgR * l2
            segment.2 = segment.2 - tmpPoint
            
            result.append(segment)
        }
        
        return result
    }
    
    private func segmentCalc(_ seg: Segment,_ t: Double) -> Point {
        let t2 = t * t
        let t3 = t2 * t
        let nt = 1.0 - t
        let nt2 = nt * nt
        let nt3 = nt2 * nt
        
        let x = nt3 * seg.0.x + 3.0 * t * nt2 * seg.1.x + 3.0 * t2 * nt * seg.2.x + t3 * seg.3.x;
        let y = nt3 * seg.0.y + 3.0 * t * nt2 * seg.1.y + 3.0 * t2 * nt * seg.2.y + t3 * seg.3.y;
        
        return Point(x: x, y: y)
    }
}
