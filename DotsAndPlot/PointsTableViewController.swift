//
//  PointsTableViewController.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 08.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import UIKit

class PointsTableViewController: UITableViewController {
    internal var presenter: PointsTablePresenterProtocol!

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfElements()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.PointsTableController.pointCellIdentifier, for: indexPath)
        cell.textLabel?.text = presenter.textForElementAtIndexPath(index: indexPath.row)
        cell.textLabel?.textAlignment = .center
        return cell
    }
}
