//
//  PlotViewController.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 08.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import UIKit

class PlotViewController: UIViewController {
    internal var presenter: PlotPresenterProtocol!
    
    @IBOutlet private weak var plotView: PlotView!
    @IBOutlet private weak var scrollView: UIScrollView!
    
    //MARK: public methods
    public func shareContentAsImage() {
        var controller: UIViewController! = nil
        if let exactImage = plotView.captureImage() {
            controller = UIActivityViewController(activityItems: [exactImage], applicationActivities: [])
        }
        else {
            let alert = UIAlertController(title: Localizations.Common.errorTitle, message: Localizations.PlotController.saveImageError, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: Localizations.Common.okButtonTitle, style: .default))
            controller = alert
        }
        present(controller, animated: true, completion: nil)
    }
    
    //MARK: UIViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.minimumZoomScale = Constants.PlotController.minimumScrollZoom
        scrollView.maximumZoomScale = Constants.PlotController.maximumScrollZoom
        scrollView.delegate = self
        plotView.dataSource = presenter
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        plotView.reloadData()
    }
}

//MARK: UIScrollViewDelegate
extension PlotViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return plotView
    }
}
