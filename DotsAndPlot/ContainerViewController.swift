//
//  ContainerViewController.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 08.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {        
    //MARK: actions
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        var plotController: PlotViewController? = nil
        for controller in children {
            if let someContr = controller as? PlotViewController {
                plotController = someContr
            }
        }
        plotController?.shareContentAsImage()
    }
}
