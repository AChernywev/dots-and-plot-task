//
//  Constants.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 10.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import UIKit

struct Constants {
    struct Resources {
        static let certificateName = "demo.bankplus.ru"
        static let certificateExtension = "cer"
    }
    struct API {
        static let domain = "demo.bankplus.ru"
        static let serverAddress = "https://demo.bankplus.ru/mobws/json/pointsList"
        static let versionKey = "version"
        static let version = "1.1"
        static let countKey = "count"
        
        static let noErrorCode = 0
        static let wrongParamsErrorCode = -100
        static let unknownErrorCode = -1
    }
    struct PlotView {
        static let onePointDelta: CGFloat = 10
        static let borderTopBottom: CGFloat = 20
        static let borderLeftRight: CGFloat = 20
        static let dotRadius: CGFloat = 3
    }
    
    struct PointsInputController {
        static let showPointsSegue = "ShowPoints"
    }
    
    struct PointsTableController {
        static let pointCellIdentifier = "PointCell"
    }
    
    struct PlotController {
        static let minimumScrollZoom: CGFloat = 1.0
        static let maximumScrollZoom: CGFloat = 3.0
    }
}
