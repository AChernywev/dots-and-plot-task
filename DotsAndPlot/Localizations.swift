// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
public enum Localizations {

  public enum Common {
    public static let errorTitle = Localizations.tr("Localizable", "Common.errorTitle")
    public static let okButtonTitle = Localizations.tr("Localizable", "Common.okButtonTitle")
  }

  public enum NetworkErrors {
    public static let unknownResponse = Localizations.tr("Localizable", "NetworkErrors.unknownResponse")
    public static let wrongParams = Localizations.tr("Localizable", "NetworkErrors.wrongParams")
  }

  public enum PlotController {
    public static let saveImageError = Localizations.tr("Localizable", "PlotController.saveImageError")
  }

  public enum PointsInputController {
    public static let descriptionText = Localizations.tr("Localizable", "PointsInputController.descriptionText")
    public static let goButtonTitle = Localizations.tr("Localizable", "PointsInputController.goButtonTitle")
    public static let noInpuDataError = Localizations.tr("Localizable", "PointsInputController.noInpuDataError")
    public static let noPointsLoadedError = Localizations.tr("Localizable", "PointsInputController.noPointsLoadedError")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension Localizations {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle = Bundle(for: BundleToken.self)
}
// swiftlint:enable convenience_type
