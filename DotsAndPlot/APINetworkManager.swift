//
//  APINetworkManager.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 09.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import Foundation
import Alamofire

//MARK: local API model objects
private struct PointModel: Decodable {
    let x: String
    let y: String
}

private struct ResponseModel: Decodable {
    let points: [PointModel]?
    let message: String?
    let result: Int?
}

private struct APIResponseModel: Decodable {
    let response: ResponseModel
    let reqID: String?
    let result: Int?
}

class APINetworkManager: NetworkManagerProtocol {
    private let session: Session
    
    //MARK: initializers
    init() {
        let pathToCert = Bundle.main.path(forResource: Constants.Resources.certificateName,
                                               ofType: Constants.Resources.certificateExtension)!
        let certificateData: NSData = NSData(contentsOfFile: pathToCert)!
        let certificate = SecCertificateCreateWithData(nil, certificateData)!
        
        let evaluator = PinnedCertificatesTrustEvaluator(certificates: [certificate],
                                         acceptSelfSignedCertificates: true,
                                             performDefaultValidation: false,
                                                         validateHost: false)
        let serverTrustPolicies = [Constants.API.domain: evaluator]
        
        let manager = ServerTrustManager(evaluators: serverTrustPolicies)
        self.session = Session(serverTrustManager: manager)
    }
    
    //MARK: public methods
    func getPoints(forQuantity quantity: Int, completion: @escaping ([Point]?, NetworkError?) -> ()) {
        let parameters = [Constants.API.versionKey : Constants.API.version, Constants.API.countKey : "\(quantity)"]
        session.request(Constants.API.serverAddress, method: .post, parameters: parameters, interceptor: nil)
        .responseDecodable(of: APIResponseModel.self) { [weak self] response in
            if let value = response.value {
                self?.handleAPIResponse(value, completion: completion)
            }
            else {
                self?.handleError(response.error, completion: completion)
            }
        }
    }
    
    //MARK: private methods
    private func handleAPIResponse(_ response: APIResponseModel, completion: ([Point]?, NetworkError?) -> ()) {
        if(response.result == Constants.API.noErrorCode) {
            completion(response.response.points?.compactMap({ model -> Point in
                let x = Double(model.x) ?? 0
                let y = Double(model.y) ?? 0
                return Point(x: x, y: y)
            }), nil)
        }
        else if(response.result == Constants.API.wrongParamsErrorCode) {
            completion(nil, NetworkError(code: response.result!, message: Localizations.NetworkErrors.wrongParams))
        }
        else {
            let errorCode = response.result ?? response.response.result ?? Constants.API.unknownErrorCode
            let message = response.response.message ?? Localizations.NetworkErrors.unknownResponse
            completion(nil, NetworkError(code: errorCode, message: message))
        }
    }
    
    private func handleError(_ apiError: AFError?, completion: ([Point]?, NetworkError?) -> ()) {
        if let error = apiError {
            completion(nil, NetworkError(code: error.responseCode ?? Constants.API.unknownErrorCode , message: error.localizedDescription))
        }
        else {
            completion(nil, NetworkError(code: Constants.API.unknownErrorCode, message: Localizations.NetworkErrors.unknownResponse))
        }
    }
}
