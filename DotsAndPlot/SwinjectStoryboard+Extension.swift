//
//  SwinjectStoryboard+Extension.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 25.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import Swinject
import SwinjectStoryboard
import SwinjectAutoregistration

extension SwinjectStoryboard {
    @objc class func setup() {
        ModulesBuilder(container: defaultContainer).preparePointsInputModule()
    }
}
