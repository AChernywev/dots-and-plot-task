//
//  ModulesBuilder.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 25.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard
import SwinjectAutoregistration

protocol ModulesBuilderProtocol {
    func preparePointsInputModule()
    func preparePointsTableModule(points: [Point])
    func preparePlotModule(points: [Point])
}

class ModulesBuilder: ModulesBuilderProtocol {    
    private let container: Container
    
    init(container: Container) {
        self.container = container
    }
    
    func preparePointsInputModule() {
        container.autoregister(NetworkManagerProtocol.self, initializer: APINetworkManager.init)
        container.autoregister(PointInputPresenterProtocol.self,
                               arguments: PointInputViewController.self, NetworkManagerProtocol.self, ModulesBuilder.self,
                               initializer: PointInputPresenter.init(view:networkManager:modulesBuilder:))
        
        container.storyboardInitCompleted(PointInputViewController.self) { resolver, controller in
            let ntwMngr = resolver.resolve(NetworkManagerProtocol.self)!
            let presenter = resolver.resolve(PointInputPresenterProtocol.self, arguments: controller, ntwMngr, self)
            controller.presenter = presenter!
        }
    }
    
    func preparePointsTableModule(points: [Point]) {
        container.autoregister(PointsTablePresenterProtocol.self, argument:[Point].self, initializer: PointsTablePresenter.init(points:))
        container.storyboardInitCompleted(PointsTableViewController.self) { resolver, controller in
            let presenter = resolver.resolve(PointsTablePresenterProtocol.self, argument:points)
            controller.presenter = presenter
        }
    }
    
    func preparePlotModule(points: [Point]) {
        container.autoregister(PlotPresenterProtocol.self, argument:[Point].self, initializer: PlotPresenter.init(points:))
        container.storyboardInitCompleted(PlotViewController.self) { resolver, controller in
            let presenter = resolver.resolve(PlotPresenterProtocol.self, argument:points)
            controller.presenter = presenter
        }
    }
}
