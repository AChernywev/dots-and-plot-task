//
//  UIView+CaptureImage.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 09.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import UIKit

//MARK: UIView capture extension
extension UIView {
    func captureImage() -> UIImage?  {
        var image: UIImage? = nil
        UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0.0)
        defer { UIGraphicsEndImageContext() }
        if let context = UIGraphicsGetCurrentContext() {
            layer.render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        return image
    }
}
