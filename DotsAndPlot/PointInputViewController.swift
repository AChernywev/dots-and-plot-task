//
//  ViewController.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 07.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import UIKit

class PointInputViewController: UIViewController {
    internal var presenter: PointInputPresenterProtocol!
    
    @IBOutlet private weak var textLabel: UILabel!
    @IBOutlet private weak var actionButton: UIButton!
    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var textFieldContainerViewBottomConstraint: NSLayoutConstraint!
    
    //MARK: UIVIewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        actionButton.setTitle(Localizations.PointsInputController.goButtonTitle, for: .normal)
        textLabel.text = Localizations.PointsInputController.descriptionText
        
        NotificationCenter.default.addObserver(self, selector: #selector(animateKeybord(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
        view.addGestureRecognizer(tap)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self);
    }
    
    //MARK: Actions
    @objc private func tapGesture(_ sender: UITapGestureRecognizer? = nil) {
        textField.resignFirstResponder()
    }
    
    @IBAction private func actionButtonPressed(_ sender: UIButton) {
        textField.resignFirstResponder()
        presenter.loadPoints(numberString: textField.text)
    }
    
    //MARK: private methods
    @objc private func animateKeybord(notification: Notification)  {
        guard let userInfo = notification.userInfo else {
            return
        }
        guard let keyboardRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let duration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let curve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue ?? 0
        
        let option = UIView.AnimationOptions(rawValue: curve << 16)
        textFieldContainerViewBottomConstraint.constant = -(view.frame.size.height - keyboardRect.origin.y)
        UIView.animate(withDuration: duration, delay: 0, options: option, animations: { [weak self] in
            self?.view.setNeedsLayout()
            self?.view.layoutIfNeeded()
        }, completion: nil)
    }
}

extension PointInputViewController: PointInputViewProtocol {
    func showError(errorMessage: String) {
        let alert = UIAlertController(title: Localizations.Common.errorTitle, message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Localizations.Common.okButtonTitle, style: .default))
        present(alert, animated: true, completion: nil)
    }
    
    func loadedPoints(points: [Point]) {
        performSegue(withIdentifier: Constants.PointsInputController.showPointsSegue, sender: points)
    }
}
