//
//  NetworkManagerProtocol.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 07.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import UIKit

//MARK: NetworkManagerProtocol
protocol NetworkManagerProtocol {
    func getPoints(forQuantity quantity: Int, completion: @escaping ([Point]?, NetworkError?) -> ())
}
