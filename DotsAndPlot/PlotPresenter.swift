//
//  PlotViewControllerAdapter.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 10.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import UIKit

protocol PlotPresenterProtocol: PlotViewDataSource {
    init(points: [Point])
}

class PlotPresenter: PlotPresenterProtocol {
    private let points: [Point]
    private let minX: Double
    private let minY: Double
    private let maxX: Double
    private let maxY: Double
    
    required init(points: [Point]) {
        var locPoints: [Point]! = nil
        let sortedPoints = points.sorted(by: {$0.x < $1.x})
        
        let spline = Spline(withControlPoints: sortedPoints)
        let count = spline.numberOfPoints
        locPoints = []
        for i in 0..<count {
            locPoints.append(spline.getPoint(atIndex: i))
        }
        
        if(locPoints.count > 0) {
            let firstPoint = locPoints[0]
            self.maxX = locPoints.reduce(firstPoint, { $0.x > $1.x ? $0 : $1}).x
            self.minX = locPoints.reduce(firstPoint, { $0.x < $1.x ? $0 : $1}).x
            self.maxY = locPoints.reduce(firstPoint, { $0.y > $1.y ? $0 : $1}).y
            self.minY = locPoints.reduce(firstPoint, { $0.y < $1.y ? $0 : $1}).y
        }
        else {
            self.maxX = 0.0
            self.minX = 0.0
            self.maxY = 0.0
            self.minY = 0.0
        }
        self.points = locPoints
    }
    
    //MARK: PlotViewDataSource methods
    func numberOfPoints(in plotView: PlotView) -> Int {
        return points.count
    }
    
    func plotView(_ plotView: PlotView, pointForIndex index: Int) -> CGPoint {
        let point = points[index]
        return CGPoint(x: point.x, y: point.y)
    }
    
    func leftBottomPoint(in plotView: PlotView) -> CGPoint {
        return CGPoint(x: minX, y: minY)
    }
    
    func rightTopPoint(in plotView: PlotView) -> CGPoint {
        return CGPoint(x: maxX, y: maxY)
    }
}
