//
//  Point.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 23.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import Foundation

//MARK: Model objects
struct Point {
    var x: Double
    var y: Double
    
    init(x: Double, y: Double) {
        self.x = x
        self.y = y
    }
    
    static var zero: Point {
        get {
            return Point(x:0, y:0)
        }
    }
    
    static func +(left: Point, right: Point) -> Point {
        let x = left.x + right.x
        let y = left.y + right.y
        
        return Point(x: x, y: y)
    }
    
    static func -(left: Point, right: Point) -> Point {
        let x = left.x - right.x
        let y = left.y - right.y
        
        return Point(x: x, y: y)
    }
    
    static func *(left: Point, value: Double) -> Point {
        let x = left.x * value
        let y = left.y * value
        
        return Point(x: x, y: y)
    }
    
    func normalizedPoint() -> Point {
        var point = self
        point.normalize()
        return point
    }
    
    mutating func normalize() {
        let length = x * x + y * y
        x = x / length
        y = y / length
    }
}

public struct NetworkError {
    let code: Int
    let message: String
}
