//
//  PointsInputPresenter.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 25.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import Foundation

protocol PointInputViewProtocol: AnyObject {
    func showError(errorMessage: String)
    func loadedPoints(points: [Point])
}

protocol PointInputPresenterProtocol {
    init(view: PointInputViewProtocol, networkManager: NetworkManagerProtocol, modulesBuilder: ModulesBuilderProtocol)
    func loadPoints(numberString: String?)
}

struct PointInputPresenter: PointInputPresenterProtocol {
    weak var view: PointInputViewProtocol?
    let networkManager: NetworkManagerProtocol
    let modulesBuilder: ModulesBuilderProtocol
    
    init(view: PointInputViewProtocol, networkManager: NetworkManagerProtocol, modulesBuilder: ModulesBuilderProtocol) {
        self.view = view
        self.networkManager = networkManager
        self.modulesBuilder = modulesBuilder
    }
    
    func loadPoints(numberString: String?) {
        if let numberOfPoints = Int(numberString ?? "") {
            networkManager.getPoints(forQuantity: numberOfPoints, completion:{ points, error in
                if let lError = error {
                    self.view?.showError(errorMessage: lError.message)
                }
                else {
                    if let locPoints = points {
                        self.pointsLoaded(points: locPoints)
                    }
                    else {
                        self.view?.showError(errorMessage: Localizations.PointsInputController.noPointsLoadedError)
                    }
                }
            })
        }
        else {
            view?.showError(errorMessage: Localizations.PointsInputController.noInpuDataError)
        }
    }
    
    private func pointsLoaded(points: [Point]) {
        modulesBuilder.preparePointsTableModule(points: points)
        modulesBuilder.preparePlotModule(points: points)
        view?.loadedPoints(points: points)
    }
}
