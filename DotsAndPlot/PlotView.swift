//
//  PlotView.swift
//  DotsAndPlot
//
//  Created by Alexandr Chernyshev on 08.09.2020.
//  Copyright © 2020 Alexandr Chernyshev. All rights reserved.
//

import UIKit

protocol PlotViewDataSource: AnyObject {
    func numberOfPoints(in plotView: PlotView) -> Int
    func plotView(_ plotView: PlotView, pointForIndex index: Int) -> CGPoint
    func leftBottomPoint(in plotView: PlotView) -> CGPoint
    func rightTopPoint(in plotView: PlotView) -> CGPoint
}

class PlotView: UIView {
    weak var dataSource: PlotViewDataSource? = nil {
        didSet {
            reloadData()
        }
    }
    
    //MARK: public methods
    func reloadData() {
        setNeedsDisplay()
    }
    
    //MARK: UIView methods
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if let context = UIGraphicsGetCurrentContext() {
            context.setLineWidth(2.0)
            context.setStrokeColor(UIColor.red.cgColor)
//            drawBorder(inRect: rect)
            
            context.setLineWidth(1.0)
            context.setStrokeColor(UIColor.black.cgColor)
            drawPath(inRect: rect)
        }
    }
    
    //MARK: private methods
    private func drawBorder(inRect rect: CGRect) {
        let path = UIBezierPath()
        let leftTopX = rect.origin.x + Constants.PlotView.borderLeftRight;
        let leftTopY = rect.origin.y + Constants.PlotView.borderTopBottom
        let rightBottomX = rect.width - Constants.PlotView.borderLeftRight
        let rightBottomY = rect.height - Constants.PlotView.borderTopBottom
        
        path.move(to: CGPoint(x: leftTopX, y: leftTopY))
        path.addLine(to: CGPoint(x: rightBottomX, y: leftTopY))
        path.addLine(to: CGPoint(x: rightBottomX, y: rightBottomY))
        path.addLine(to: CGPoint(x: leftTopX, y: rightBottomY))
        path.addLine(to: CGPoint(x: leftTopX, y: leftTopY))
        path.stroke()
    }
    
    private func drawPath(inRect rect: CGRect) {
        guard let localDataSource = dataSource else {
            return
        }
        
        let count = localDataSource.numberOfPoints(in: self)
        if (count > 0) {
            let path = UIBezierPath()
            let firstPoint = transformToViewCoordinates(point: localDataSource.plotView(self, pointForIndex: 0),inRect: rect)
            path.move(to: firstPoint)
            
            if (count == 1) {
                path.addArc(withCenter: firstPoint, radius: Constants.PlotView.dotRadius, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
                path.fill()
            }
            else {
                for i in 0..<count {
                    let point = transformToViewCoordinates(point: localDataSource.plotView(self, pointForIndex: i), inRect: rect)
                    path.addLine(to: point)
                }
                path.stroke()
            }
        }
    }
    
    private func transformToViewCoordinates(point: CGPoint, inRect rect: CGRect) -> CGPoint {
        guard let localDataSource = dataSource else {
            return CGPoint(x: 0, y: 0)
        }
        
        let count = localDataSource.numberOfPoints(in: self)
        var leftBottomPoint = localDataSource.leftBottomPoint(in: self)
        var rightTopPoint = localDataSource.rightTopPoint(in: self)
        
        if(count == 1) {
            leftBottomPoint.x -= Constants.PlotView.onePointDelta
            leftBottomPoint.y -= Constants.PlotView.onePointDelta
            rightTopPoint.x += Constants.PlotView.onePointDelta
            rightTopPoint.y += Constants.PlotView.onePointDelta
        }
        
        let transformX = (rightTopPoint.x - leftBottomPoint.x) / (rect.width - rect.origin.x - 2 * Constants.PlotView.borderLeftRight)
        let transformY = (rightTopPoint.y - leftBottomPoint.y) / (rect.height - rect.origin.y - 2 * Constants.PlotView.borderTopBottom)
        
        let newX = Constants.PlotView.borderLeftRight + (point.x - leftBottomPoint.x) / transformX
        let newY = Constants.PlotView.borderTopBottom - (point.y - rightTopPoint.y) / transformY
        
        return CGPoint(x: newX, y: newY)
    }
}
